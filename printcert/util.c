#include "rfc3280.h"

#include "util.h"

#define BUFFER_LENGTH 4096

A2C_ERROR _A2C_PrintToMemory(PCVOID pv, PC_A2C_DESCRIPTOR pdesc, PBYTE* return_data)
{
    A2C_STREAM* printStream_ptr = NULL;
    A2C_ERROR err = A2C_ERROR_Success;
    PBYTE data = NULL;
    int data_length = 0;

    err = A2C_CreateMemoryStream(&printStream_ptr);
    if (err < A2C_ERROR_Success)
    {
        return err;
    }

    err = A2C_Print(pv, pdesc, printStream_ptr);
    if (err < A2C_ERROR_Success)
    {
        return err;
    }

    err = A2C_GetDataFromStream(printStream_ptr, &data, &data_length);
    if (err < A2C_ERROR_Success)
    {
        return err;
    }

    *return_data = data;
    return err;
}

static A2C_ERROR _A2C_DecodeBerFile(PVOID * ppv, PC_A2C_DESCRIPTOR pdesc, int flags, FILE* file)
{
    unsigned char buffer[BUFFER_LENGTH];
    int return_code = 0;
    A2C_CONTEXT* pcxt = NULL;
    A2C_ERROR err = A2C_ERROR_Success;

    while (1)
    {
        return_code = fread(buffer, 1, sizeof(buffer), file);

        if (return_code < 0)
        {
            err = A2C_ERROR_malformedEncoding;
            break;
        }

        if (return_code == 0)
        {
            break;
        }

        err = A2C_DecodeBer(
            ppv,
            pdesc,
            flags,
            &pcxt,
            buffer,
            return_code
            );
        if ((err < A2C_ERROR_Success) && (err != A2C_ERROR_needMoreData))
        {
            break;
        }

        flags |= A2C_FLAGS_MORE_DATA;
    }

    if ((err < A2C_ERROR_Success) && (pcxt != NULL))
    {
        /* A2C_FreeContext(pcxt); */
    }

    return err;
}

Cert_Certificate* read_certificate_from_file(const char* filename)
{
    A2C_ERROR err = A2C_ERROR_Success;
    Cert_Certificate* certificate = NULL;
    FILE* file = NULL;

    file = fopen(filename, "rb");
    if (file == NULL)
    {
        perror(filename);
        return NULL;
    }

    err = _A2C_DecodeBerFile(
        (PVOID*) &certificate,
        &Cert_Certificate_descriptor,
        0,
        file
        );

    (void) fclose(file);

    if (err < A2C_ERROR_Success)
    {
        return NULL;
    }

    return certificate;
}
