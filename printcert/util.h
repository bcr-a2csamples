#ifndef UTIL_H
#define UTIL_H

#define _A2C_OIDS_EQUAL(o1, o2) (A2C_Compare(o1, o2, &A2C_OBJECT_IDENTIFIER_descriptor) == 0)

A2C_ERROR _A2C_PrintToMemory(PCVOID pv, PC_A2C_DESCRIPTOR pdesc, PBYTE* return_data);

Cert_Certificate* read_certificate_from_file(const char* filename);

#endif /* UTIL_H */
