#include "rfc3280.h"

#include "util.h"

static void print_attribute_value(A2C_OPEN_TYPE* value)
{
    PBYTE string = NULL;

    if (_A2C_PrintToMemory(value->objectPtr, value->functionPtr, &string) == A2C_ERROR_Success)
    {
        printf("%s\n", string);

        free(string);
    }
}

static void print_name_email_addresses(Cert_Name* name)
{
    int counter = 0;

    for (counter = 0;counter < name->u.rdnSequence.count;++counter)
    {
        int rdn_counter = 0;

        for (rdn_counter = 0;rdn_counter < name->u.rdnSequence.array[counter].count;++rdn_counter)
        {
            Cert_AttributeTypeAndValue* attribute = &(name->u.rdnSequence.array[counter].array[rdn_counter]);

            if (_A2C_OIDS_EQUAL(&(attribute->type), Cert_emailAddress.id))
            {
                print_attribute_value(&attribute->value);
            }
        }
    }
}

static void print_subject_alt_name_email_addresses(Cert_Certificate* certificate)
{
    int counter = 0;
    
    for (counter = 0;counter < certificate->toBeSigned.extensions.count;++counter)
    {
        if (_A2C_OIDS_EQUAL(&certificate->toBeSigned.extensions.array[counter].extnID, &CertImpl_id_ce_subjectAltName))
        {
            CertImpl_GeneralNames* names = NULL;
            A2C_ERROR err = A2C_ERROR_Success;
            int names_counter = 0;

            err = A2C_DecodeBer(
                (PVOID*) &names,
                &CertImpl_GeneralNames_descriptor,
                0,
                NULL,
                certificate->toBeSigned.extensions.array[counter].extnValue.data,
                certificate->toBeSigned.extensions.array[counter].extnValue.length
                );
            if (err < A2C_ERROR_Success)
            {
                return;
            }

            for (names_counter = 0;names_counter < names->count;++names_counter)
            {
                if (names->array[names_counter].index == CertImpl_GeneralName_rfc822Name_index)
                {
                    printf("%s\n", names->array[names_counter].u.rfc822Name.string);
                }
            }

            (void) A2C_Free_CertImpl_GeneralNames(names);
        }
    }
}

int main(int argc, char** argv)
{
    Cert_Certificate* certificate = NULL;

    certificate = read_certificate_from_file(argv[1]);
    if (certificate != NULL)
    {
        print_name_email_addresses(&certificate->toBeSigned.subject);
        print_subject_alt_name_email_addresses(certificate);
    }
}
