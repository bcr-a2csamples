printcert is a (somewhat misnamed) sample that will print out all of the
email addresses in a certificate. Specifically, it prints out:

* Any email address found in an emailAddress attribute of the subject DN

* Any email address found in an rfc822Name element in a subjectAltName
  extension.

To build it, there is a Makefile. I imagine that this will work for any gcc
environment. Non-Cygwin Windows people, well, sorry. I'd certainly take any
contribution of a DSP / DSW file if you'd like to provide one.

You will need to have built the A2C library, and $PATH_TO_A2C needs to be set.

When you've gotten it built, you can run the unit tests by making the "all"
or "check" target.

~/Source/OpenSource/bcr-a2csamples/printcert$ make all
cc -I/Users/blake/Source/OpenSource/a2c/runtime/C    printcert.c util.c rfc3280.c /Users/blake/Source/OpenSource/a2c/runtime/C/A2C_Runtime.lib   -o printcert
cd test && ./runtests && cd ..
+PASS AliceDSSSignByCarlNoInherit
+PASS WildID

Some potentially interesting things:

* util.c has some helper functions and macros that might be useful.
  Specifically:

  * _A2C_OIDS_EQUAL -- checks if two OIDs are equal

  * _A2C_PrintToMemory -- does an A2C_Print on an object, returns the result
    in a memory block

  * read_certificate_from_file -- reads in a certificate from a disk file
